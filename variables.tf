variable "security_group_ids" {
  description = "Security Group IDs to assign to the instance"
}

variable "vpc_subnet_id" {
  description = "The VPC Subnet ID to assign the instance in the correct VPC"
}

variable "server_name" {
  description = "This is the name to attach to your EC2 instance."
}

variable "myaws_keypair" {
  description = "name of existing aws keypair to supply"
  default     = "beau-uswest1"
}
